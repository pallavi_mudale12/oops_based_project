public abstract class TwoDimensionalShape extends Shape implements IArea, IPerimeter {
	
	int length;
	int breadth;

	public abstract void CalculatePerimeter();

	public abstract void CalculateArea();
}
