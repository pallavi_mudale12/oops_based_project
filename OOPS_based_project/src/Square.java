
public class Square extends TwoDimensionalShape {
	@Override
	public void CalculateArea() {
		System.out.println("Square Area: " +(length*length));	
	}

	@Override
	public void CalculatePerimeter() {
		System.out.println("Square Perimeter: "+4*length);
	}
}
