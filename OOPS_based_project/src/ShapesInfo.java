
public class ShapesInfo {
	public static void main(String[] args) {
		Square square = new Square();
		square.Shape = "Square";
		square.Name = "Rubix cube";
		square.Color = "Mutli Color";
		square.length=8;
		
		System.out.println("square parameters:");
		square.GetName();
		square.GetShape();
		square.GetColor();
		square.CalculateArea();
		square.CalculatePerimeter();
		
		System.out.println();
		
		Line line = new Line();
		line.LineLength = 20;
		line.Name = "Thin Line";
		line.Color = "black";
		line.Shape = "Curve";
		
		System.out.println("line parameters:");
		line.GetName();
		line.GetShape();
		line.GetColor();
		line.CalculateLength();
	}
}
