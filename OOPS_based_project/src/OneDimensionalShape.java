
public abstract class OneDimensionalShape extends Shape {
	public abstract void CalculateLength();
}
